import scrapy
import datetime

class SpeechesSpider(scrapy.Spider):
    name = "speeches"


    def start_requests(self):
        # Hillary and Trump campaign speeches links
        urls = [
            "http://www.presidency.ucsb.edu/2016_election_speeches.php?candidate=70&campaign=2016CLINTON&doctype=5000",
            "http://www.presidency.ucsb.edu/2016_election_speeches.php?candidate=45&campaign=2016TRUMP&doctype=5000"
        ]
        debates_urls = ["http://www.presidency.ucsb.edu/ws/index.php?pid=119039",
                        "http://www.presidency.ucsb.edu/ws/index.php?pid=119038",
                        "http://www.presidency.ucsb.edu/ws/index.php?pid=118971"
        ]
        debate_dates = [
            datetime.datetime(2016, 10, 19).strftime("%B %d, %Y"),
            datetime.datetime(2016, 10, 9).strftime("%B %d, %Y"),
            datetime.datetime(2016, 9, 26).strftime("%B %d, %Y"),
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse_speeches_list)
        for i, url in enumerate(debates_urls):
            req = scrapy.Request(url,callback=self.parse_speeches)
            req.meta["name"] = "debate"
            req.meta["date"] = debate_dates[i]
            req.meta["descr"] = "Debate between Trump and Clinton"
            yield req

    def parse_speeches_list(self, response):
        min_date = datetime.datetime(2015, 4, 1)
        link_table = response.xpath("//table[count(tr)>10 and @width>200]").css("tr")

        # We remove the first row that is is header
        for descr in link_table[1:]:
            texts = descr.css("td::text").extract()
            speech_date = datetime.datetime.strptime(texts[1], "%B %d, %Y")

            if speech_date > min_date:
                req = scrapy.Request(response.urljoin(descr.css("td a::attr(href)").extract_first()), callback=self.parse_speeches)
                req.meta["name"]=texts[0]
                req.meta["date"]=texts[1]
                req.meta["descr"]=descr.css("td a::text").extract_first()
                yield req

    def parse_speeches(self, response):
        header_text = response.xpath("//span[@class='displaytext']/text()").extract()
        remaining_text = response.xpath("//span[@class='displaytext']/p").extract()

        output_dict = {}
        output_dict["name"] = response.meta["name"]
        output_dict["date"] = response.meta["date"]
        output_dict["descr"] = response.meta["descr"]
        header_text.extend(remaining_text)
        output_dict["text"] = "\n".join(header_text)
        output_dict["link"] = response.url
        yield output_dict


