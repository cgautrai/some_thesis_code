package controllers

import java.io.File

import play.api._
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._
import play.api.mvc._
import play.mvc.Http

import reactivemongo.api._
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.api.indexes.Index
import reactivemongo.api.indexes.IndexType.Ascending
import reactivemongo.bson.BSONDocument
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

import play.api.libs.json._

import scala.io.Source

object Application extends Controller {

  case class Customer(clientId: Long, baskets: List[Basket], signature: List[Product], top_k: Option[List[Product]], blocs: Option[List[List[Long]]])
  case class Basket(timestamp: Long, products: List[Product])
  case class Product(price: Option[Double], quantity: Option[Int], brand: Option[String], articleId: Long,
                     articleLabel: Option[String], segmentId: Option[Int], segmentLabel: Option[String],
                     subFamilyId: Option[Int], subFamilyLabel: Option[String], familyId: Option[Int],
                     familyLabel: Option[String], sectionId: Option[Int], sectionLabel: Option[String])

  implicit val productReads: Reads[Product] = (
    (JsPath \ "price").readNullable[Double] and
    (JsPath \ "quantity").readNullable[Int] and
    (JsPath \ "brand").readNullable[String] and
    (JsPath \ "prodId").read[Long] and
    (JsPath \ "prodLabel").readNullable[String] and
    (JsPath \ "segmentId").readNullable[Int] and
    (JsPath \ "segmentLabel").readNullable[String] and
    (JsPath \ "subFamilyId").readNullable[Int] and
    (JsPath \ "subFamilyLabel").readNullable[String] and
    (JsPath \ "familyId").readNullable[Int] and
    (JsPath \ "familyLabel").readNullable[String] and
    (JsPath \ "sectionId").readNullable[Int] and
    (JsPath \ "sectionLabel").readNullable[String]
    )(Product.apply _)

  implicit val basketReads: Reads[Basket] = (
    (JsPath \ "timestamp").read[Long] and
    (JsPath \ "products").read[List[Product]]
  )(Basket.apply _)

  implicit val customerReads: Reads[Customer] = (
    (JsPath \ "clientId").read[Long] and
    (JsPath \ "baskets").read[List[Basket]] and
    (JsPath \ "signature").read[List[Product]] and
    (JsPath \ "top_k").readNullable[List[Product]] and
    (JsPath \ "blocs").readNullable[List[List[Long]]]
  )(Customer.apply _)

  implicit val productWrites = new Writes[Product] {
    def writes(prod: Product) = Json.obj(
        "price" -> prod.price,
        "quantity" -> prod.quantity,
        "brand" -> prod.brand,
        "prodId" -> prod.articleId,
        "prodLabel" -> prod.articleLabel,
        "segmentId" -> prod.segmentId,
        "segmentLabel" -> prod.segmentLabel,
        "subFamilyId" -> prod.subFamilyId,
        "subFamilyLabel" -> prod.subFamilyLabel,
        "familyId" -> prod.familyId,
        "familyLabel" -> prod.familyLabel,
        "sectionId" -> prod.sectionId,
        "sectionLabel" -> prod.sectionLabel
    )
  }

  implicit val basketWrites = new Writes[Basket] {
    def writes(basket: Basket) = Json.obj(
      "timestamp" -> basket.timestamp,
      "products" -> basket.products
    )
  }

  implicit val customerWrites = new Writes[Customer] {
    def writes(cust: Customer) = Json.obj(
      "clientId" -> cust.clientId,
      "baskets" -> cust.baskets,
      "signature" -> cust.signature,
      "blocs" -> cust.blocs,
      "top_k" -> cust.top_k)
  }


  def index = Action{
    Ok(views.html.index("ok"))
  }

  def mongoTest = Action {
    val driver = new MongoDriver
    val connection = driver.connection(List("localhost"))

    // Gets a reference to the database "plugin"
    val signatures_db = connection("customer_signatures")
    val collNames = Await.result(signatures_db.collectionNames,2.seconds).mkString(",")

    val signaturesColl = signatures_db.collection[BSONCollection]("client_file_with_signature_segment_20pct")
    signaturesColl.indexesManager.ensure(new Index(Seq(("clientId",Ascending))))

    val allClients = signaturesColl.find(BSONDocument("clientId" -> BSONDocument("$gte" -> 0))).cursor[BSONDocument]().collect[List](1)

    Ok(views.html.index("nb clients: "  + Await.result(allClients, 60.seconds).length.toString))
  }

  def uploadCustomer = Action{
    Ok(views.html.uploadCustomer.render())
  }

  def showCustomer = Action(parse.multipartFormData) {
    request =>
      request.body.file("customer").map { customer =>
      customer.ref.moveTo(new File("public/data/last_customer.json"), true)

      val jsonContent = Source.fromFile("public/data/last_customer.json", "windows-1252").getLines().mkString

      val json = Json.parse(jsonContent)

      json.validate[Customer] match {
        case s: JsSuccess[Customer] => {
          val cust: Customer = s.get
          println("Customer id " + cust.clientId)
          Ok(views.html.showCustomer(cust))
        }
        case e: JsError => {
          Redirect(routes.Application.uploadCustomer).flashing(
            "error" -> "Error parsing json"
          )
        }
      }
    }.getOrElse {
      Redirect(routes.Application.uploadCustomer).flashing(
        "error" -> "Missing file"
      )
    }
  }

}