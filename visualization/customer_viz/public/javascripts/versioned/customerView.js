/**
 * Created by cgautrai on 28/10/2015.
 */
function CustomerView(customer, blockId){
    var that={};

    that.customer = customer;
    that.customer.baskets.sort(function compare(a, b) {
        if (a.timestamp < b.timestamp)
            return -1;
        if (a.timestamp > b.timestamp)
            return 1;
        return 0;
    });

    // Contains the block where we are going to draw the customer view
    that.baseBlockId = d3.select("body")
    if(!(typeof blockId === "undefined")){
        that.baseBlockId = d3.select("#"+blockId);
    }

    that.draw = function(){
        that.buildProductColorScale();
        that.buildPeriodColorScale();
        that.drawCustomerId();
        that.drawPurchaseRythm();
        that.drawBasketsContent();
        that.drawEnrichedBaskets();
    }

    that.groupingFunction = function(prod){
        if(prod.segmentId != 0 && prod.segmentId != 9999)
            return prod.segmentLabel;
        else{
            return prod.familyLabel;
        }
    }

    that.drawCustomerId = function(){
        that.baseBlockId.append("h2").text(that.customer.clientId)
    }

    that.buildProductColorScale = function(){
        var productIdentifierList = {};
        for(idBasket in that.customer.baskets){
            for(idProduct in that.customer.baskets[idBasket].products){
                if(that.groupingFunction(that.customer.baskets[idBasket].products[idProduct]) in productIdentifierList)
                    productIdentifierList[that.groupingFunction(that.customer.baskets[idBasket].products[idProduct])] += 1;
                else
                    productIdentifierList[that.groupingFunction(that.customer.baskets[idBasket].products[idProduct])] = 1;
            }
        }

        console.log(productIdentifierList);

        if(d3.keys(productIdentifierList).length <= 20){
            that.productColorScale = d3.scale.category20();
            that.productColorScale.domain(d3.map(productIdentifierList, function(d){return d.key;}).keys());
        }
        else {
            var sortedProds = d3.entries(productIdentifierList).sort(function compare(a, b) {
                if (a.value < b.value)
                    return 1;
                if (a.value > b.value)
                    return -1;
                return 0;
            });
            that.productColorScale = d3.scale.category20();
            that.productColorScale.domain(d3.map(sortedProds.slice(0,20), function(d){return d.key;}).keys());
            console.log(that.productColorScale.domain());
        }
    }

    that.buildPeriodColorScale = function(){
        that.separators = [];

        for(id in that.customer.blocs){
            // We take the last one as separator
            that.separators.push(that.customer.blocs[id][1])
        }

        that.periodColorScale = d3.scale.category10();
        //that.periodColorScale = d3.scale.ordinal().range(["red", "green"]);
    }

    that.getSeparatorId = function(timestamp){
        for(id in that.separators){
            if(timestamp <= that.separators[id])
                return id;
        }
        return that.separators.length;
    }

    that.drawBasket = function(basket, parentNode, cellSize){
        if(parentNode == undefined)
        parentNode = that.baseBlockId;

        var root = groupBasketBy(basket, that.groupingFunction);

        var margin = {top: 0, right: 0, bottom: 0, left: 0},
            width = cellSize - margin.left - margin.right,
            height = cellSize - margin.top - margin.bottom;

        var treemap = d3.layout.treemap()
            .size([width, height])
            .sticky(true)
            .value(function(d) { return 1; });

        var group = parentNode.append("g")
            .attr("width", (width + margin.left + margin.right) + "px")
            .attr("height", (height + margin.top + margin.bottom) + "px")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        var node = group.datum(root).selectAll(".node")
            .data(treemap.nodes)
            .enter().append("rect")
            .attr("class", "node")
            .call(position)
            .attr("fill", function(d) {
                if(d.children)
                    return (that.productColorScale.domain().indexOf(d.name) >= 0) ? that.productColorScale(d.name) : "#FDF5E6";
                else
                    return null;
    })
            .attr("opacity", function(d){return d.children ? "1.0" : "0";})
            .append("title")
            .text(function(d) { return d.children ? null : d.prodLabel; });
    };

    that.drawPurchaseRythm = function(){
        var format = d3.time.format("%Y-%m-%d"), formatYear=d3.time.format("%Y");

        // Transform data to fit the required format : key is the formatted date and value the list of products
        var dataArray = d3.nest().key(function(b){return format(new Date(b.timestamp));})
            .entries(that.customer.baskets);

        var data = {};
        for(id in dataArray)
        {
            if(dataArray[id].values.length == 1) {
                data[dataArray[id].key] = dataArray[id].values[0].products;
            }
            else {
                data[dataArray[id].key] = dataArray[id].values.reduce(function (b1, b2) {
                    return union_arrays_prods(b1.products, b2.products)
                });
            }
        }

        // Draw the heatmap
        var width = 960,
            height = 136,
            cellSize = 17; // cell size

        var margin = {left:45};

        // Create color range depending on the number of product bought
        var color = d3.scale.quantile()
            .domain(d3.range(d3.min(d3.values(data), function(d){return d.length;}), d3.max(d3.values(data), function(d){return d.length;})))
            .range(colorbrewer.YlGn[9]);

        // Draw framework
        var svg = that.baseBlockId.append("div").selectAll("svg")
            .data(d3.range(parseInt(d3.min(d3.keys(data), function(d){return formatYear(format.parse(d));})),
                parseInt(d3.max(d3.keys(data), function(d){return formatYear(format.parse(d));})) + 1))
            .enter().append("svg")
            .attr("width", width + margin.left)
            .attr("height", height)
            .append("g")
            .attr("transform", "translate(" + ((width - cellSize * 53) / 2 + margin.left) + "," + (height - cellSize * 7 - 1) + ")");

        svg.append("text")
            .attr("transform", "translate(-45," + cellSize * 3.5 + ")rotate(-90)")
            .style("text-anchor", "middle")
            .text(function(d) { return d; });

        // Adds rect for days
        var rect = svg.selectAll(".day")
            .data(function(d) { return d3.time.days(new Date(d, 0, 1), new Date(d + 1, 0, 1)); })
            .enter().append("rect")
            .attr("class", "day")
            .attr("fill", "#fff")
            .attr("width", cellSize)
            .attr("height", cellSize)
            .attr("transform", function(d){return "translate(" + d3.time.weekOfYear(d) * cellSize + "," + d.getDay() * cellSize + ")";})
            .datum(format);

        rect.append("title")
            .text(function(d) { return d; });

        svg.selectAll(".month")
            .data(function(d) { return d3.time.months(new Date(d, 0, 1), new Date(d + 1, 0, 1)); })
            .enter().append("path")
            .attr("class", "month")
            .attr("d", monthPath);

        rect.filter(function(d) {return d in data; })
            .attr("fill", function(d) {return color(data[d].length); })
            .select("title")
            .text(function(d) { return d + ": " + data[d].length + " products"; });

        // Draw the legend: month and days of week
        var month = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        var week_days = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];

        var legend = svg.selectAll(".legend")
            .data(month)
            .enter().append("g")
            .attr("class", "legend")
            .attr("transform", function(d, i) { return "translate(" + (((i+1) * width/13 - cellSize/2)) + ",0)"; });

        legend.append("text")
            .attr("class", function(d,i){ return month[i] })
            .style("text-anchor", "end")
            .attr("dy", "-.25em")
            .text(function(d,i){ return month[i] });

        for (var i=0; i<7; i++)
        {
            svg.append("text")
                .attr("transform", "translate(-5," + cellSize*(i+1) + ")")
                .style("text-anchor", "end")
                .attr("dy", "-.25em")
                .text(function(d) { return week_days[i]; });
        }

        function monthPath(t0) {
            var t1 = new Date(t0.getFullYear(), t0.getMonth() + 1, 0),
                d0 = t0.getDay(), w0 = d3.time.weekOfYear(t0),
                d1 = t1.getDay(), w1 = d3.time.weekOfYear(t1);
            return "M" + (w0 + 1) * cellSize + "," + d0 * cellSize
                + "H" + w0 * cellSize + "V" + 7 * cellSize
                + "H" + w1 * cellSize + "V" + (d1 + 1) * cellSize
                + "H" + (w1 + 1) * cellSize + "V" + 0
                + "H" + (w0 + 1) * cellSize + "Z";
        }

        d3.select(self.frameElement).style("height", "2910px");
    }


    that.drawBasketsContent= function(){
        var format = d3.time.format("%Y-%m-%d"), formatYear=d3.time.format("%Y");

        // Transform data to fit the required format : key is the formatted date and value the list of products
        var dataArray = d3.nest().key(function(b){return format(new Date(b.timestamp));})
            .entries(that.customer.baskets);

        var data = {};
        for(id in dataArray)
        {
            if(dataArray[id].values.length == 1) {
                data[dataArray[id].key] = dataArray[id].values[0].products;
            }
            else {
                data[dataArray[id].key] = dataArray[id].values.reduce(function (b1, b2) {
                    return union_arrays_prods(b1.products, b2.products)
                });
            }
        }

        // Draw the heatmap
        var cellSize = 34;
        var width = 12*4.5*cellSize,
            height = 300;

        var margin = {left:45};

        // Draw framework
        var svg = that.baseBlockId.append("div").selectAll("svg")
            .data(d3.range(parseInt(d3.min(d3.keys(data), function(d){return formatYear(format.parse(d));})),
                parseInt(d3.max(d3.keys(data), function(d){return formatYear(format.parse(d));})) + 1))
            .enter().append("svg")
            .attr("width", width + margin.left)
            .attr("height", height)
            .append("g")
            .attr("transform", "translate(" + ((width - cellSize * 53) / 2 + margin.left) + "," + (height - cellSize * 7 - 1) + ")");

        svg.append("text")
            .attr("transform", "translate(-45," + cellSize * 3.5 + ")rotate(-90)")
            .style("text-anchor", "middle")
            .text(function(d) { return d; });

        // Adds rect for days
        var groups = svg.selectAll(".day")
            .data(function(d) { return d3.time.days(new Date(d, 0, 1), new Date(d + 1, 0, 1)); })
            .enter().append("g")
            .attr("class", "day")
            .attr("fill", "#fff")
            .attr("width", cellSize)
            .attr("height", cellSize)
            .attr("transform", function(d) { return "translate(" + d3.time.weekOfYear(d) * cellSize + "," + d.getDay() * cellSize + ")"; })
            .datum(format);

        svg.selectAll(".month")
            .data(function(d) { return d3.time.months(new Date(d, 0, 1), new Date(d + 1, 0, 1)); })
            .enter().append("path")
            .attr("class", "month")
            .attr("d", monthPath);

        groups.append("rect")
            .attr("class", "day")
            .attr("fill", "#fff")
            .attr("width", cellSize)
            .attr("height", cellSize);

        groups.filter(function(d) {if(d in data){that.drawBasket({timestamp:new Date(d).getTime(), products:data[d]}, d3.select(this), cellSize)} return true; });

        // Draw the legend: month and days of week
        var month = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        var week_days = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];

        var legend = svg.selectAll(".legend")
            .data(month)
            .enter().append("g")
            .attr("class", "legend")
            .attr("transform", function(d, i) { return "translate(" + (((i+1) * width/13 - cellSize/2)) + ",0)"; });

        legend.append("text")
            .attr("class", function(d,i){ return month[i] })
            .style("text-anchor", "end")
            .attr("dy", "-.25em")
            .text(function(d,i){ return month[i] });

        for (var i=0; i<7; i++)
        {
            svg.append("text")
                .attr("transform", "translate(-5," + cellSize*(i+1) + ")")
                .style("text-anchor", "end")
                .attr("dy", "-.25em")
                .text(function(d) { return week_days[i]; });
        }

        function monthPath(t0) {
            var t1 = new Date(t0.getFullYear(), t0.getMonth() + 1, 0),
                d0 = t0.getDay(), w0 = d3.time.weekOfYear(t0),
                d1 = t1.getDay(), w1 = d3.time.weekOfYear(t1);
            return "M" + (w0 + 1) * cellSize + "," + d0 * cellSize
                + "H" + w0 * cellSize + "V" + 7 * cellSize
                + "H" + w1 * cellSize + "V" + (d1 + 1) * cellSize
                + "H" + (w1 + 1) * cellSize + "V" + 0
                + "H" + (w0 + 1) * cellSize + "Z";
        }

        d3.select(self.frameElement).style("height", "2910px");
    }

    that.drawEnrichedBaskets = function(){
        that.drawTopkAndSignInBaskets();
        //that.drawSignatureInBaskets();
        //that.drawTopkInBaskets();
        that.drawBasketsTimeline();
        that.drawSeparators();
        that.drawBasketsHtml();
    }

    that.getFormattedBasketData = function(){
        var format = d3.time.format("%Y-%m-%d"), formatYear=d3.time.format("%Y");

        // Transform data to fit the required format : key is the formatted date and value the list of products
        var dataArray = d3.nest().key(function(b){return format(new Date(b.timestamp + 60*1000));})
            .entries(that.customer.baskets);

        var data = {};
        for(id in dataArray)
        {
            if(dataArray[id].values.length == 1) {
                data[dataArray[id].key] = dataArray[id].values[0].products;
            }
            else {
                data[dataArray[id].key] = dataArray[id].values.reduce(function (b1, b2) {
                    return union_arrays_prods(b1.products, b2.products)
                });
            }
        }
        return data;
    }

    that.getBasketColorScale = function(data){
        return d3.scale.quantile()
            .domain(d3.range(d3.min(d3.values(data), function(d){return d.length;}), d3.max(d3.values(data), function(d){return d.length;})))
            .range(colorbrewer.YlGn[9]);
    }

    that.drawBasketsTimeline = function(){
        /************************************
         * **********************************
         * Draws the timeline of basket
         *
         * **********************************
         ************************************/
        var format = d3.time.format("%Y-%m-%d"), formatYear=d3.time.format("%Y");

        var data = that.getFormattedBasketData();

        // Draw the heatmap
        var widthCalendar = 960,
            heightCalendar = 136,
            cellSize = 17; // cell size

        var marginCalendar = {left:45};

        // Create color range depending on the number of product bought
        var color = that.getBasketColorScale(data);

        // Draw framework
        var svg = that.baseBlockId.append("div").attr("id", "prodsDraw").selectAll("svg")
            .data(d3.range(parseInt(d3.min(d3.keys(data), function(d){return formatYear(format.parse(d));})),
                parseInt(d3.max(d3.keys(data), function(d){return formatYear(format.parse(d));})) + 1))
            .enter().append("svg")
            .attr("width", widthCalendar + marginCalendar.left)
            .attr("height", heightCalendar)
            .append("g")
            .attr("transform", "translate(" + ((widthCalendar - cellSize * 53) / 2 + marginCalendar.left) + "," + (heightCalendar - cellSize * 7 - 1) + ")");

        svg.append("text")
            .attr("transform", "translate(-45," + cellSize * 3.5 + ")rotate(-90)")
            .style("text-anchor", "middle")
            .text(function(d) { return d; });

        // Adds rect for days
        var rect = svg.selectAll(".day")
            .data(function(d) { return d3.time.days(new Date(d, 0, 1), new Date(d + 1, 0, 1)); })
            .enter().append("rect")
            .attr("class", "dayProds")
            //.attr("fill", "#fff")
            .attr("fill", function(d) {return that.periodColorScale(that.getSeparatorId(d.getTime())); })
            .attr("opacity", 0.3)
            //.attr("stroke", "#ccc")
            .attr("stroke", function(d){return that.periodColorScale(that.getSeparatorId(d.getTime()));})
            .attr("width", cellSize)
            .attr("height", cellSize)
            .attr("transform", function(d){return "translate(" + d3.time.weekOfYear(d) * cellSize + "," + d.getDay() * cellSize + ")";})
            .datum(format)

        rect.append("title")
            .text(function(d) { return d; });

        svg.selectAll(".month")
            .data(function(d) { return d3.time.months(new Date(d, 0, 1), new Date(d + 1, 0, 1)); })
            .enter().append("path")
            .attr("class", "month")
            .attr("d", monthPath);

        rect.filter(function(d) {return d in data; })
            //.attr("fill", function(d) {return color(data[d].length); })
            .attr("fill", function(d) {return that.periodColorScale(that.getSeparatorId(format.parse(d).getTime())); })
            .attr("opacity", 1)
            .select("title")
            .text(function(d) { return d + ": " + data[d].length + " products"; });

        // Draw the legend: month and days of week
        var month = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        var week_days = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];

        var legend = svg.selectAll(".legend")
            .data(month)
            .enter().append("g")
            .attr("class", "legend")
            .attr("transform", function(d, i) { return "translate(" + (((i+1) * widthCalendar/13 - cellSize/2)) + ",0)"; });

        legend.append("text")
            .attr("class", function(d,i){ return month[i] })
            .style("text-anchor", "end")
            .attr("dy", "-.25em")
            .text(function(d,i){ return month[i] });

        for (var i=0; i<7; i++)
        {
            svg.append("text")
                .attr("transform", "translate(-5," + cellSize*(i+1) + ")")
                .style("text-anchor", "end")
                .attr("dy", "-.25em")
                .text(function(d) { return week_days[i]; });
        }

        function monthPath(t0) {
            var t1 = new Date(t0.getFullYear(), t0.getMonth() + 1, 0),
                d0 = t0.getDay(), w0 = d3.time.weekOfYear(t0),
                d1 = t1.getDay(), w1 = d3.time.weekOfYear(t1);
            return "M" + (w0 + 1) * cellSize + "," + d0 * cellSize
                + "H" + w0 * cellSize + "V" + 7 * cellSize
                + "H" + w1 * cellSize + "V" + (d1 + 1) * cellSize
                + "H" + (w1 + 1) * cellSize + "V" + 0
                + "H" + (w0 + 1) * cellSize + "Z";
        }

        d3.select(self.frameElement).style("height", "2910px");
    }

    /**
     * The function draws a treemap of product link with the basket timeline drawn in html id prodsDraw.
     *
     * div is the div object to draw into.
     * root is the data needed to draw the treemap (with name and children attribute
     */
    that.drawLinkedProductTreemap = function(root, div, prodList){
        var data = that.getFormattedBasketData();
        var color = that.getBasketColorScale(data);
        var format = d3.time.format("%Y-%m-%d");

        var signSize = 120;
        var margin = {top: 10, right: 10, bottom: 10, left: 10},
            width = signSize - margin.left - margin.right,
            height = signSize - margin.top - margin.bottom;

        var treemap = d3.layout.treemap()
            .size([width, height])
            .sticky(true)
            .value(function(d) { return 1; });

        var group = div.append("svg")
            .attr("width", (width + margin.left + margin.right) + "px")
            .attr("height", (height + margin.top + margin.bottom) + "px")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        var node = group.datum(root).selectAll(".node")
            .data(treemap.nodes)
            .enter().append("rect")
            .attr("class", "node")
            .call(position)
            .attr("fill", function(d) {
                return (that.productColorScale.domain().indexOf(d.name) >= 0) ? that.productColorScale(d.name) : "#FDF5E6";
            })
            .on("mouseover", function(d, id){
                var associatedSvg = d3.select("#prodsDraw");
                if(id <= 0)
                    return;
                var prodId = prodList[id-1].prodId;
                console.log(prodId);
                associatedSvg.selectAll(".dayProds").filter(function(p){
                    if(!(p in data))
                        return false;
                    for(idProdBasket in data[p]){
                        if(data[p][idProdBasket].prodId == prodId)
                            return false;
                    }
                    return true;
                })
                    .attr("opacity", 0.3)
                    //.attr("fill", "#fff")

                /******
                 * We do the same for separators plots

                var associatedSepSvg = d3.select("#separators");
                associatedSepSvg.selectAll(".sepRect").filter(function(p){
                        if(!(p in data))
                            return false;
                        for(idProdBasket in data[p]){
                            if(data[p][idProdBasket].prodId == prodId)
                                return false;
                        }
                        return true;
                    })
                    .attr("opacity", 0.3)*/

            })
            .on("mouseout", function(){
                var associatedSvg = d3.select("#prodsDraw");
                associatedSvg.selectAll(".dayProds").filter(function(d) {return d in data; })
                    //.attr("fill", function(d) {return color(data[d].length); })
                    //.attr("fill", function(d) {return that.periodColorScale(that.getSeparatorId(format.parse(d).getTime())); })
                    .attr("opacity", 1)
            })
            .append("title")
            .text(function(d) { return d.name; });

        group.append("text")
            .text(root.name)
            .attr("transform", "translate(" + width/2 +"," + signSize +")")
            .attr("text-anchor", "middle");
    }

    /**
     * The function will draw top k and signature content on the same div, and link it to the basket chart.
     */
    that.drawTopkAndSignInBaskets = function(){
        /**********************
         * First, we draw the signature
         *
         *********************/
        // Draw the signature as a basket
        var signatureGroups = [];
        var signatureGroupingIds = [];
        var prodListSign = [];
        for(id in that.customer.signature) {
            signatureGroups.push({name: that.groupingFunction(that.customer.signature[id])});
            signatureGroupingIds.push(that.groupingFunction(that.customer.signature[id]));
            prodListSign.push(that.customer.signature[id]);
        }

        var root = {name:"signature", children:signatureGroups};
        console.log(root);

        var div = that.baseBlockId.append("div");

        that.drawLinkedProductTreemap(root, div, prodListSign);

        /*******************************************
         * And then we draw products that are in top-k and not in signature
         * *******************************************
         *******************************************/
        var top_kGroups = [];
        var prodListTopk = [];
        for(id in that.customer.top_k) {
            if(signatureGroupingIds.indexOf(that.groupingFunction(that.customer.top_k[id])) < 0) {
                top_kGroups.push({name: that.groupingFunction(that.customer.top_k[id])});
                prodListTopk.push(that.customer.top_k[id]);
            }
        }

        var root_topk = {name:"top_k", children:top_kGroups};
        console.log(root_topk);
        that.drawLinkedProductTreemap(root_topk, div, prodListTopk);

    }

    that.drawSignatureInBaskets = function(){
        // Draw the signature as a basket
        var signatureGroups = [];
        var prodList = []
        for(id in that.customer.signature) {
            signatureGroups.push({name: that.groupingFunction(that.customer.signature[id])});
            prodList.push(that.customer.signature[id]);
        }

        var root = {name:"signature", children:signatureGroups};
        console.log(root);

        var div = that.baseBlockId.append("div");

        that.drawLinkedProductTreemap(root, div, prodList);
    }

    that.drawTopkInBaskets = function(){
        // Draw the top-k as a basket
        var top_kGroups = [];
        var prodList = [];
        console.log(that.customer.top_k);
        for(id in that.customer.top_k) {
            top_kGroups.push({name: that.groupingFunction(that.customer.top_k[id])});
            prodList.push(that.customer.top_k[id]);
        }

        var root = {name:"top_k", children:top_kGroups};
        console.log(root);

        var div = that.baseBlockId.append("div");

        that.drawLinkedProductTreemap(root, div, prodList);
    }

    that.drawSeparators = function(){
        var format = d3.time.format("%Y-%m-%d");
        var data = that.getFormattedBasketData();
        // Draw the heatmap
        var width = 960,
            height = 136;

        var xscale = d3.time.scale()
            .domain([d3.min(that.customer.blocs, function(a){return a[0];}), d3.max(that.separators)])
            .range([0, width]);

        // Draw framework
        var svg = that.baseBlockId.append("div").attr("id", "separators")
            .append("svg")
            .attr("width", width)
            .attr("height", height)
            .append("g");

        svg.selectAll("rect")
            .data(that.separators)
            .enter().append("rect")
            .attr("class", "sepRect")
            .attr("width", function(d,i){return xscale(d)-xscale(that.customer.blocs[i][0]);})
            .attr("height", 30)
            .attr("x", function(d, i){return xscale(that.customer.blocs[i][0]);})
            .attr("y", 20)
            .attr("fill", function(d,i){return that.periodColorScale(i);})
            .on("mouseover", function(d, id){
                var associatedSvg = d3.select("#prodsDraw");
                associatedSvg.selectAll(".dayProds").filter(function(p){
                        if(!(p in data))
                            return false;
                        var currentDate = format.parse(p).getTime();
                        var min_date = 0;
                        if(id > 0)
                            min_date = that.customer.blocs[id-1][1];
                        console.log(currentDate);
                        console.log(d);

                        return !(currentDate <= d && currentDate > min_date);
                    })
                    .attr("opacity", 0.5);
                //.attr("fill", "#fff")
            })
            .on("mouseout", function(){
                var associatedSvg = d3.select("#prodsDraw");
                associatedSvg.selectAll(".dayProds").filter(function(d) {return d in data; })
                    //.attr("fill", function(d) {return color(data[d].length); })
                    //.attr("fill", function(d) {return that.periodColorScale(that.getSeparatorId(format.parse(d).getTime())); })
                    .attr("opacity", 1)
            })

        var xAxis = d3.svg.axis()
            .orient("bottom")
            .scale(xscale);

        svg.append("g")
            .attr("class", "xaxis")   // give it a class so it can be used to select only xaxis labels  below
            .attr("transform", "translate(0," + (height - 50) + ")")
            .attr("width", width)
            .call(xAxis);
    }

    that.drawBasketsHtml = function(){
        /**
         * ********************************
         * ********************************
         * Adds basket content as html list
         * ********************************
         * ********************************
         */
        var format = d3.time.format("%Y-%m-%d");
        var basketContentDiv = that.baseBlockId.append("div");
        basketContentDiv.selectAll("ul")
            .data(that.customer.baskets)
            .enter()
            .append("ul")
            .attr("name", function (d) {
                return format(new Date(d.timestamp + 60*1000))
            })
            .html(function (d) {
                return "<h2>" + format(new Date(d.timestamp + 60*1000)) + "</h2>"
            })
            .selectAll("li")
            .data(function (b) {
                return b.products
            })
            .enter()
            .append("li")
            .text(function (p) {
                var text = "";
                if (p.prodLabel) {
                    text += p.prodLabel
                } else {
                    text += p.prodId
                }

                if (p.segmentLabel) {
                    text += "/" + p.segmentLabel
                }
                if (p.subFamilyLabel) {
                    text += "/" + p.subFamilyLabel
                }
                if (p.familyLabel) {
                    text += "/" + p.familyLabel
                }
                return text;
            })
    }

    return that;
}

/**
 * Groups the product of a basket according to a grouping function. The result is then formatted to fit into a treemap.
 * @param basket : object of type {timestamp:..., products:[...Product]} where products is a list of product
 * @param groupingFunction : A function that maps a product to a value (string or int for exemple)
 * @returns An object with the form {name:<timestamp>, children:[{name : <groupingFunction(...)>, children:[...Product]}]}
 */
function groupBasketBy(basket, groupingFunction){
    var groupedBasket = d3.nest().key(groupingFunction).entries(basket.products);

    var basketNodes = {};
    basketNodes.name = basket.timestamp;
    basketNodes.children = [];

    for(id in groupedBasket)
    {
        elem = groupedBasket[id];
        var basketNode = {};
        basketNode.name = elem.key;
        basketNode.children = elem.values;
        basketNodes.children.push(basketNode);
    }
    return basketNodes;
}

function position() {
    this.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; })
        .attr("width", function(d) { return Math.max(0, d.dx - 1) + "px"; })
        .attr("height", function(d) { return Math.max(0, d.dy - 1) + "px"; });
}

function union_arrays_prods (x, y) {
    var union = x;

    if(union == undefined)
        union = [];

    for(id in y)
    {
        if(!union.find(function(p){return p.prodId == y[id].prodId})){
            union.push(y[id])
        }
    }
    return union;
}