package fr.liglab.datalyse.retail.algorithms;

import fr.liglab.datalyse.retail.BasketToDataset;
import fr.liglab.datalyse.retail.Client;
import fr.liglab.datalyse.retail.access.ClientAccessor;
import fr.liglab.datalyse.retail.access.IndexedFileClientAccessor;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.*;

/**
 * Created by cgautrai on 08/06/2016.
 */
public class SignatureDynProgMaxTest {
    ClientAccessor clientAccessor;
    SignatureDynamicProgMax productFinder;

    /*@Before
    public void setUp() throws IOException {
        clientAccessor = new IndexedFileClientAccessor("/udd/cgautrai/git/RETAIL_viz/public/data/clients.data", "/udd/cgautrai/git/RETAIL_viz/public/data/clients.index");
    }*/

    @Test
    public void testBasicSignature1seg() {
        productFinder = new SignatureDynamicProgMax("transac.db", 1);

        List<Set<Integer>> signature = productFinder.getSocle();
        String strSignature = buildSignaturePrint(signature);

        System.out.println(strSignature);
        org.junit.Assert.assertEquals(new HashSet<Integer>(Arrays.asList(1,2,3,4,5,7,8,9,10)), signature.get(0));
    }

    @Test
    public void testBasicSignature2segs() {
        productFinder = new SignatureDynamicProgMax("transac.db", 2);

        List<Set<Integer>> signature = productFinder.getSocle();
        String strSignature = buildSignaturePrint(signature);

        System.out.println(strSignature);
        org.junit.Assert.assertEquals(new HashSet<Integer>(Arrays.asList(1,3,4,7)), signature.get(0));
    }

    @Test
    public void testBasicSignature2segsOther() {
        productFinder = new SignatureDynamicProgMax("transac2.db", 2);

        List<Set<Integer>> signature = productFinder.getSocle();
        String strSignature = buildSignaturePrint(signature);

        System.out.println(strSignature);
        org.junit.Assert.assertEquals(new HashSet<Integer>(Arrays.asList(1,2,3,4,5)), signature.get(0));
    }

    @Test
    public void testBasicSignature3segs() {
        productFinder = new SignatureDynamicProgMax("transac.db", 3);

        List<Set<Integer>> signature = productFinder.getSocle();
        String strSignature = buildSignaturePrint(signature);

        System.out.println(strSignature);
        org.junit.Assert.assertEquals(new HashSet<Integer>(Arrays.asList(1)), signature.get(0));
    }

    @Test
    public void testBasicSignature3segsOther() {
        productFinder = new SignatureDynamicProgMax("transac2.db", 3);

        List<Set<Integer>> signature = productFinder.getSocle();
        String strSignature = buildSignaturePrint(signature);

        System.out.println(strSignature);
        org.junit.Assert.assertEquals(new HashSet<Integer>(Arrays.asList(1,2,3,4)), signature.get(0));
    }

    @Test
    public void testBasicSignature4segs() {
        productFinder = new SignatureDynamicProgMax("transac.db", 4);

        List<Set<Integer>> signature = productFinder.getSocle();
        String strSignature = buildSignaturePrint(signature);

        System.out.println(strSignature);
        org.junit.Assert.assertEquals(new HashSet<Integer>(Arrays.asList(1)), signature.get(0));
    }

    @Test
    public void testBasicSignature4segsOther() {
        productFinder = new SignatureDynamicProgMax("transac2.db", 4);

        List<Set<Integer>> signature = productFinder.getSocle();
        String strSignature = buildSignaturePrint(signature);

        System.out.println(strSignature);
        org.junit.Assert.assertEquals(0, signature.size());
    }

    @Test
    public void testBasicSignature4segsOptimal() {
        productFinder = new SignatureDynamicProgMax("transac_opt.db", 4);

        List<Set<Integer>> signature = productFinder.getSocle();
        String strSignature = buildSignaturePrint(signature);

        System.out.println(strSignature);
        org.junit.Assert.assertEquals(new HashSet<Integer>(Arrays.asList(4,5)), signature.get(0));
    }

    @Test
    public void testBasicSignatureDifferentSigns() {
        productFinder = new SignatureDynamicProgMax("transac3.db", 3);

        List<Set<Integer>> signature = productFinder.getSocle();
        String strSignature = buildSignaturePrint(signature);

        System.out.println(strSignature);
        org.junit.Assert.assertTrue(signature.contains(new HashSet<Integer>(Arrays.asList(1,2,3,4,5))));
        org.junit.Assert.assertTrue(signature.contains(new HashSet<Integer>(Arrays.asList(1,2,3,6,7))));
    }

    private String buildSignaturePrint(List<Set<Integer>> signature){
        String strSignature = "";
        for(Set<Integer> s : signature) {
            strSignature += "[";
            for (Integer p : s) {
                strSignature += p.toString() + ",";
            }
            strSignature+="],";
        }
        return strSignature;
    }

//    @Test
//    public void testProfiler(){
//            productFinder = new SignatureDynamicProgMax("C:\\Users\\cgautrai\\workspace\\repo_these\\temp_signature_dataset", 3);
//            List<Set<Integer>> signature = productFinder.getSocle();
//    }
}
