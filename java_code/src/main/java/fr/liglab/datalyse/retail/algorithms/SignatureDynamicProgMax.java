package fr.liglab.datalyse.retail.algorithms;

import fr.liglab.datalyse.retail.BasketToDataset;
import fr.liglab.datalyse.retail.Product;
import fr.liglab.datalyse.retail.access.DatabaseFileReader;
import fr.liglab.jlcm.internals.TransactionReader;

import gnu.trove.map.hash.TIntObjectHashMap;

import java.io.IOException;
import java.util.*;

/**
 * Created by cgautrai on 08/06/2016.
 */
public class SignatureDynamicProgMax {
    TIntObjectHashMap<Product> itemMap;
    private List<Set<Integer>> matrix;
    private int minSep;
    // Contains the DP table
    private List<List<List<Set<Integer>>>> costs;
    //Contains the segment used for the DP table
    private List<List<List<Integer>>> segmentsOpt;
    private List<Set<Integer>> socle;

    /***
     *
     * @param transactions:
     * @param nbSeps: Number of separators. CAUTION: this acts as a MAX sep in getSkyline
     *              (kinda stupid but hey, we'll see if I have time to do better).
     */
    public SignatureDynamicProgMax(BasketToDataset transactions, int nbSeps) {
        matrix = new ArrayList<Set<Integer>>();
        itemMap = transactions.getPatternsConversion();

        // Recover item data from client baskets
        Iterator<TransactionReader> transacs = transactions.iterator();
        while (transacs.hasNext()) {
            TransactionReader transac = transacs.next();
            Set<Integer> row = new HashSet<Integer>();
            while (transac.hasNext()) {
                int item = transac.next();
                row.add(item);
            }
            matrix.add(row);
        }

        this.minSep = nbSeps;

        this.costs = new ArrayList<List<List<Set<Integer>>>>();
        this.segmentsOpt = new ArrayList<List<List<Integer>>>();
        this.socle = new ArrayList<Set<Integer>>();

        this.findSign();
    }

    /***
     *
     * @param dataBasePath
     * @param minSep: Minimum number of separators. CAUTION: this acts as a MAX sep in getSkyline
     *              (kinda stupid but hey, we'll see if I have time to do better).
     */
    public SignatureDynamicProgMax(String dataBasePath, double minSep) {

        matrix = new ArrayList<Set<Integer>>();

        // Recover item data from client baskets
        try {
            Iterator<String> transacs = DatabaseFileReader.getTransactions(dataBasePath).iterator();
            while (transacs.hasNext()) {
                String transac = transacs.next();
                List<Integer> row = fr.liglab.datalyse.retail.access.TransactionReader.getTransactionItems(transac);
                matrix.add(new HashSet<Integer>(row));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(minSep<1)
            this.minSep = (int)(matrix.size()*minSep);
            // Init separators
        else
            this.minSep = (int)minSep;

        this.costs = new ArrayList<List<List<Set<Integer>>>>();
        this.segmentsOpt = new ArrayList<List<List<Integer>>>();
        this.socle = new ArrayList<Set<Integer>>();

        this.findSign();
    }


    public void buildDynamicMatrix(){
        // First, we initialize the matrix
        costs.add(new ArrayList<List<Set<Integer>>>());
        segmentsOpt.add(new ArrayList<List<Integer>>());

        Iterator<Set<Integer>> it = matrix.iterator();
        List<Set<Integer>> segments = new ArrayList<Set<Integer>>();
        segments.add(new HashSet<Integer>());
        int index=0;
        while(it.hasNext()){
            Set<Integer> transac = it.next();
            segments.get(0).addAll(transac);
            costs.get(0).add(new ArrayList<Set<Integer>>(Arrays.asList(costFunction(segments))));
            segmentsOpt.get(0).add(new ArrayList<Integer>(Arrays.asList(index)));
            index++;
        }

        //And now we fill the matrix !
        // We iterate over the number of blocs
        for(int l=1; l<this.minSep; l++){
            // We add a column in the matrix and fill it thereafter
            costs.add(new ArrayList<List<Set<Integer>>>());
            segmentsOpt.add(new ArrayList<List<Integer>>());
            for(int i=0; i<matrix.size(); i++){
                List<Integer> opts = new ArrayList<Integer>();
                int max = 0;
                List<Set<Integer>> maxCost = new ArrayList<Set<Integer>>();
                //List<List<Integer>> bestSeps = new ArrayList<List<Integer>>();
//                Set<Integer> rowsConsidered = rowsUnion(l,i);
                for(int j=l-1; j<i; j++){
//                    List<Set<Integer>> cost = findBestIntersections(rowsConsidered, costs.get(l-1).get(j));
//                    rowsConsidered.removeAll(matrix.get(j+1));
                    List<Set<Integer>> cost = findBestIntersections(rowsUnion(j+1,i), costs.get(l-1).get(j));

                    for(Set<Integer> c : cost){
                        boolean included = false;
                        for(int m=0; m<maxCost.size(); m++){
                            if(maxCost.get(m).containsAll(c)){
                                included = true;
                                break;
                            }
                            else if(c.containsAll(maxCost.get(m))) {
                                maxCost.remove(m);
//                                bestSeps.remove(m);
                                m--;
                            }
                        }
                        if(!included){
                            maxCost.add(c);
                        }
                    }
                }
                segmentsOpt.get(l).add(opts);
                costs.get(l).add(maxCost);
            }
        }
    }

    private List<Set<Integer>> findBestIntersections(Set<Integer> segment, List<Set<Integer>> partialSigns){
        List<Set<Integer>> bestSets = new ArrayList<Set<Integer>>();
        // Index of the partialSeps corresponding to the partialSign in the loop
        for(Set<Integer> partialSign : partialSigns){
            Set<Integer> segmentCopy = new HashSet<Integer>(segment);
            segmentCopy.retainAll(partialSign);

            boolean included = false;
            for(int i =0; i<bestSets.size(); i++){
                // If the current set is included, we do not add it
                if(bestSets.get(i).containsAll(segmentCopy)){
                    included = true;
                    break;
                }
                // If the current set is a super set of an existing element, we remove the exiting element
                else if(segmentCopy.containsAll(bestSets.get(i))){
                    bestSets.remove(i);
                    i--;
                }
            }
            if(!included){
                bestSets.add(segmentCopy);
            }
        }
        return bestSets;
    }

    public void findSign(){
        this.buildDynamicMatrix();
        if(costs.get(this.minSep-1).get(matrix.size()-1).size() > 0 &&
                costs.get(this.minSep-1).get(matrix.size()-1).get(0).size() >0 ){
            List<Set<Integer>> currentSetsUnfiltered = costs.get(this.minSep-1).get(matrix.size()-1);
            List<Set<Integer>> currentSets = new ArrayList<Set<Integer>>();
            // We remove the intermediate wrong results
            int maxSize = 0;
            for(Set<Integer> s : currentSetsUnfiltered){
                if(s.size() > maxSize) {
                    maxSize = s.size();
                    currentSets.clear();
                }
                if(s.size() == maxSize) {
                    currentSets.add(s);
                }
            }
            this.socle = currentSets;
        }
        else
            this.socle = new ArrayList<Set<Integer>>();
    }

    /***
     * We compute the skyline from the DP table (costs)
     * The min_sep parameter acts here as a max sep: The skyline contains all levels where nbSeps < min_seps
     * @return
     */
    public Collection<List<SocleBloc>> getSkyline() {
        List<Set<Integer>> currentSets = new ArrayList<Set<Integer>>();
        List<List<SocleBloc>> skyline = new ArrayList<List<SocleBloc>>();
        for(int i=0; i< this.minSep; i++){
            List<Set<Integer>> currentSetsUnfiltered = costs.get(i).get(matrix.size()-1);

            // We remove the intermediate wrong results
            int maxSize = 0;
            for(Set<Integer> s : currentSetsUnfiltered){
                if(s.size() > maxSize) {
                    maxSize = s.size();
                    currentSets.clear();
                }
                if(s.size() == maxSize) {
                    currentSets.add(s);
                }
            }

            if(currentSets.size() > 0)
                skyline.add(new ArrayList<SocleBloc>());
            for(Set<Integer> s : currentSets){
                SocleBloc socle = new SocleBloc(getBlocs(s), s);
                // If we dominate the previous items in the skyline, we remove them
                // We do -2 on index because we just added an array above...
                if(skyline.size()>1 && skyline.get(skyline.size()-2).get(0).getSocle().size() <= s.size()){
                    skyline.remove(skyline.size()-2);
                }
                skyline.get(skyline.size()-1).add(socle);
            }
        }
        return skyline;
    }

    /***
     * Returns the blocs associated with the given set of products.
     * @param s
     * @return The list of blocs associated with the set of integers
     */
    public List<Bloc> getBlocs(Set<Integer> s){
        List<Bloc> blocs = new ArrayList<Bloc>();
        int b_begin=0, b_end = -1, i=0;
        Set<Integer> currentSet = new HashSet<Integer>();
        for(Set<Integer> basket: matrix){
            currentSet.addAll(basket);
            // Once we got all items, we have a bloc !
            if(currentSet.containsAll(s)){
                b_end = i+1;
                blocs.add(new Bloc(b_begin, b_end));
                currentSet.clear();
                b_begin = i+1;
            }
            i++;
        }
        // The last bloc ends at the end of the database
        if(blocs.size()>0){
            blocs.get(blocs.size()-1).setEnd(this.matrix.size());
        }
        return blocs;

    }

    /***
     * The cost function to maximize.
     * For now, it is the segments intersection length
     * @param segments
     * @return
     */
    public Set<Integer> costFunction(List<Set<Integer>> segments){
        Set<Integer> intersection = new HashSet<Integer>(segments.get(0));
        Iterator<Set<Integer>> it = segments.iterator();
        while(it.hasNext()){
            intersection.retainAll(it.next());
        }
        return intersection;
    }

    /***
     * Makes the union of the row with index in [start,end]
     * @param start
     * @param end
     * @return
     */
    Set<Integer> rowsUnion(int start, int end) {

        Set<Integer> result = new HashSet<Integer>();
        for (int i = start; i <= end; ++i) {
            result.addAll(matrix.get(i));
        }
        return result;

//		return IntStream.range(start, end)
//					.stream()
//				matrix.stream().reduce(new HashSet<Integer>(), (partialUnion, row) -> {
//			partialUnion.addAll(row);
//			return partialUnion;
//		});
    }

    public List<Set<Integer>> getSocle(){
        return this.socle;
    }
}
