# Provides basic functions to use the nomenclature
segments = {}
def get_segment(id_prod, nomenclature):
    """
    Returns the segment label of the product id. If not found, returns the sub-family label or family label.
    :param id_prod: The product id
    :param nomenclature: The nomenclature, as json. Loaded from the nomenclature export from the cluster.
    :return:
    """
    if id_prod in segments.keys():
        return segments[id_prod]
    for p in nomenclature:
        if p["prodId"] == id_prod:
            if p["segmentLabel"] and not "FICTI" in p["segmentLabel"]:
                segments[id_prod] = p["segmentLabel"]
                return p["segmentLabel"]
            elif p["subFamilyLabel"] and not "FICTI" in p["subFamilyLabel"]:
                segments[id_prod] = p["subFamilyLabel"]
                return p["subFamilyLabel"]
            elif p["familyLabel"] and not "FICTI" in p["familyLabel"]:
                segments[id_prod] = p["familyLabel"]
                return p["familyLabel"]
    segments[id_prod] = str(id_prod)
    return str(id_prod)


# We define a function to get the family of a product
families = {}
def get_family(id_prod, nomenclature):
    """
    Returns the family label of the product id. If not found, returns family id.
    :param id_prod: The product id
    :param nomenclature: The nomenclature, as json. Loaded from the nomenclature export from the cluster.
    :return:
    """
    if id_prod in families.keys():
        return families[id_prod]
    for p in nomenclature:
        if p["prodId"] == id_prod:
            if p["familyLabel"]:
                families[id_prod] = p["familyLabel"]
                return p["familyLabel"]
            else:
                families[id_prod] = str(p["familyId"])
                return str(p["familyId"])
    families[id_prod] = str(id_prod)
    return str(id_prod)

# We define a function to get the family of a product
products={}
def get_product(id_prod, nomenclature):
    """
    Returns the product object of the product id.
    :param id_prod: The product id
    :param nomenclature: The nomenclature, as json. Loaded from the nomenclature export from the cluster.
    :return:
    """
    if id_prod in products.keys():
        return products[id_prod]
    for p in nomenclature:
        if p["prodId"] == id_prod:
            products[id_prod] = p
            return p
    return {}