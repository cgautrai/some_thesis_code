import os, subprocess
import collections
import math
from random import randint
from code_these_clement.parsers.skyline_computation_parser import SkylineComputationParser
# This file contains functions to compute signatures


def compute_sky_signature(baskets, signature_jar_path, min_sup, runner="fr.liglab.datalyse.retail.runners.RunSkySignatureComputation"):
    """
    Computes the sky-signature for the given list of baskets.
    :param baskets: A list of baskets. Each basket is a dict with 2 keys : timestamp and products. Products is a list of products id.
        It can also be a file name corresponding to a dataset. The dataset is formatted as follows: Each transaction is a line, with comma separated items id.
    :param signature_jar_path: The path to the signature jar
    :param min_sup: The minimal support to use for the periodic computation (can be relative if below 1)
    :param runner: The runner to use. 4 choices available for now:
        fr.liglab.datalyse.retail.runners.RunSkySignatureComputation,
        fr.liglab.datalyse.retail.runners.RunSkySignatureDPComputation,
        fr.liglab.datalyse.retail.runners.RunSignatureTopDownComputation and
        fr.liglab.datalyse.retail.runners.RunSkySignatureDPMaxComputation
    :return: The skyline, as a list of dict. The first key of the dict is blocs and contains a list of tuples. Each tuple is a bloc.
        The second key is signature and contains the signature content associated with the blocs.
    """
    working_dir = os.getcwd()
    dataset_base_name = "temp_signature_dataset" + str(randint(0,100))
    dataset_name = os.path.join(working_dir, dataset_base_name)
    output_base_name = "temp_signature_result" + str(randint(0,100))
    output_name = os.path.join(working_dir, output_base_name)

    # If we are given a list of baskets, we create a temporary dataset
    if not isinstance(baskets, str):
        with open(dataset_name, "w+") as dataset:
            for b in baskets:
                dataset.write(",".join([str(p) for p in b['products']]) + "\n")
        if min_sup < 1:
            min_sup = int(len(baskets) * min_sup)
    # If we have a filename, we use the file as the dataset
    else:
        dataset_name = baskets
        if min_sup < 1:
            acc = 0
            with open(dataset_name) as d:
                for line in d:
                    acc += 1
            min_sup = int(min_sup*acc)

    ret_code = subprocess.call(["java", "-cp", os.path.abspath(signature_jar_path),
                     runner,
                     "-input", os.path.abspath(dataset_name),
                     "-minBlocks", str(min_sup),
                     "-output", output_name])
    skyline = []
    if ret_code == 0:
        with open(output_name, "r") as output:
            for line in output:
                if len(line.rstrip().split(":")) > 1:
                    blocs = [list(map(int, b.split(";"))) for b in line.rstrip().split(":")[0].split(",")]
                    items = list(map(int, line.rstrip().split(":")[1].split(",")))
                    skyline.append({"blocs": blocs, "signature": items})

    return skyline

def compute_sky_signature_hybrid(baskets, signature_jar_path, optimal_index=-1, optimal_dp=False):
    """
    Computes the sky-signature for the given list of baskets. The theoretically optimal switch between dynamic programming and pattern mining is used.
    :param baskets: A list of baskets. Each basket is a dict with 2 keys : timestamp and products. Products is a list of products id.
        It can also be a file name corresponding to a dataset. The dataset is formatted as follows: Each transaction is a line, with comma separated items id.
    :param signature_jar_path: The path to the signature jar
    :param optimal_index: The index used to split the sky signature computation
    :param optimal_dp: If set to True, uses the optimal DP algorithm
    :return: The skyline, as a list of dict. The first key of the dict is blocs and contains a list of tuples. Each tuple is a bloc.
        The second key is signature and contains the signature content associated with the blocs.
    """

    runner_dp = "fr.liglab.datalyse.retail.runners.RunSkySignatureDPMaxComputation" if optimal_dp else "fr.liglab.datalyse.retail.runners.RunSkySignatureDPComputation"

    baskets.sort(key=lambda x: x["timestamp"])
    cust_baskets = [b for b in baskets if b["products"]]
    i = 0
    while i < len(cust_baskets) - 1:
        if cust_baskets[i]["timestamp"] == cust_baskets[i + 1]["timestamp"]:
            cust_baskets[i]["products"] = cust_baskets[i]["products"] + cust_baskets[i + 1]["products"]
            del cust_baskets[i + 1]
        else:
            i += 1

    cust_baskets.sort(key=lambda x: x["timestamp"])
    if optimal_index <= 0:
        complexities = []
        prod_counts = collections.Counter([p for b in cust_baskets for p in b["products"]])
        for k in range(1, max(list(prod_counts.values())) + 1):
            complexities.append(2**(len([p for p in prod_counts.values() if p >= k])) + k * len(cust_baskets) ** 2)
        optimal_index = complexities.index(min(complexities)) + 1
    skyDP = compute_sky_signature(cust_baskets, signature_jar_path, optimal_index,
                                  runner=runner_dp)

    skyPatterns = compute_sky_signature(cust_baskets, signature_jar_path, optimal_index,
                                        runner="fr.liglab.datalyse.retail.runners.RunSkySignatureComputation")

    skyDP.extend(skyPatterns)

    modified_sky = []
    # We process blocs to make coherent across computations
    for s in skyDP:
        new_blocs = SkylineComputationParser.get_blocs(s["signature"], baskets)
        timestamp_list = [b["timestamp"] for b in baskets]
        new_blocs = [[timestamp_list.index(b[0]), timestamp_list.index(b[1])] for b in new_blocs]
        if new_blocs:
            new_blocs[-1][1] = len(timestamp_list)-1
        modified_sky.append({"signature":s["signature"], "blocs":new_blocs})

    return modified_sky






