import os, subprocess
# This file contains functions needed to execute periodic patterns execution


def compute_periodics(baskets, per_miner_path, min_sup, grouping_days=30):
    """
    Computes periodic patterns for the given list of baskets.
    :param baskets: A list of baskets. Each basket is a dict with 2 keys : timestamp and products. Products is a list of products id.
    :param per_miner_path: The path to the Perminer executable
    :param min_sup: The minimal support to use for the periodic computation (can be relative if below 1)
    :param grouping_days: The number of days to use in each window to group the baskets. If no basket is found in a window, a blank line is inserted. 0 means that the transactions are not grouped.
    :return: The periodic patterns, represented as a dict with period as key, and list of list of products as value.
            Each list of products is period pattern with period equal to the key.
    """
    total_windows=0
    # First, we transform the dataset to match the format used by perminer
    # We define a temporary file to store the dataset
    with open("temp_perminer.data", "w+") as temp_dataset:
        time_interval = grouping_days*1000*60*60*24  # Time interval is in days
        old_timestamp = -1
        current_prods = []
        for basket in baskets:
            timestamp = basket["timestamp"]
            prods = [str(p) for p in basket["products"]]
            if old_timestamp == -1:
                old_timestamp = timestamp
                current_prods = prods

            if len(current_prods) > 0 and (timestamp-old_timestamp) >= time_interval:
                if time_interval > 0:
                    # We compute the number of intervals that we passed to print empty lines if needed
                    nb_intervals = int((timestamp-old_timestamp)/time_interval)
                    total_windows += nb_intervals
                    # The timestamp now begins at the beginning of the last window
                    old_timestamp += nb_intervals*time_interval

                    # We print the products and then empty lines if needed
                    for i in range(nb_intervals):
                        if i == 0:
                            temp_dataset.write(" ".join(sorted(current_prods)) + "\n")
                            current_prods = prods
                        else:
                            temp_dataset.write("\n")
                else:
                    temp_dataset.write(" ".join(sorted(current_prods)) + "\n")
                    current_prods = []
                    old_timestamp = -1
            # If we are not at the end of a window, we aggregate the baskets iteratively
            else:
                current_prods = list(set().union(prods, current_prods))
        # For the last window, we print it even though it is not finished
        if current_prods:
            temp_dataset.write(" ".join(sorted(current_prods)))
            total_windows += 1

    # Then, we run perminer on the dataset we just created
    periodic_patterns = {}

    if min_sup < 1:
        min_sup = int(min_sup * total_windows)

    command = per_miner_path + " " + os.path.abspath(temp_dataset.name) + " " + str(min_sup)

    try:
        output = subprocess.check_output(command, stderr=subprocess.STDOUT)
    except Exception as e:
        output = str(e.output)
    finished = ""
    if output:
        finished = output.decode("utf-8").split('\n')
    for line in finished:
        period = -1
        if len(line.split(" ")) > 1:
            period = int(line.split(" ")[1].split(",")[0])
        current_prods = [int(p) for p in line.split(" ")[0].split(",") if p]
        if period not in periodic_patterns.keys():
            periodic_patterns[period] = []
        periodic_patterns[period].append(current_prods)

    return periodic_patterns


def compute_periodics_union(baskets, per_miner_path, min_sup, grouping_days=30):
    """
    Computes the union of periodic patterns for the given list of baskets.
    :param baskets: A list of baskets. Each basket is a dict with 2 keys : timestamp and products. Products is a list of products id.
    :param per_miner_path: The path to the Perminer executable
    :param min_sup: The minimal support to use for the periodic computation (can be relative if below 1)
    :param grouping_days: The number of days to use in each window to group the baskets. If no basket is found in a window, a blank line is inserted. 0 means that the transactions are not grouped.
    :return: The union of periodic patterns, represented as a list of products
    """
    periodics = compute_periodics(baskets, per_miner_path, min_sup, grouping_days)
    # Then we flatten the list and return the set
    return list(set(prod for l in periodics.values() for prod in l ))