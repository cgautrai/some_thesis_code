import sys

sys.path.append("/home/cgautrai/these")

from code_these_clement.utils.signature_func import compute_sky_signature
from code_these_clement.parsers import skyline_computation_parser
import json
import warnings
import collections
import math
import os
import datetime
import argparse
warnings.filterwarnings("ignore")

# We set up the experiments in here and save it in the right place
now = datetime.datetime.now()
expe_dir_name = "./experiments/" + "sky_sign_analysis_" + \
           str(now.year) + "_" + str(now.month) + "_" + str(now.day) + "_" +str(now.hour) + "_" + str(now.minute)
if not os.path.exists(expe_dir_name):
    os.makedirs(expe_dir_name)

results_dir = os.path.join(expe_dir_name, "results")
if not os.path.exists(results_dir):
    os.makedirs(results_dir)

signature_jar_path = "/home/cgautrai/these/code_these_clement/resources/signatures.jar"

expe_params = {}
expe_params['nb_custs'] = 50000
# expe_params['file_directory'] = r"/home/cgautrai/data/customer_inter/150000_15baskets_0.15_segment_baskets"
expe_params['file_directory'] = r"/home/cgautrai/data/customers_instacart/"

expe_params_file = os.path.join(expe_dir_name, "expe_params.txt")
with open(expe_params_file, "w") as f:
    json.dump(expe_params, f)

# skylines_file = os.path.join(expe_params['file_directory'], "skySignatures.txt")
skylines_file = os.path.join(expe_params['file_directory'], "50000_custs_prod_ids.jsonl")

parser = skyline_computation_parser.SkylineComputationParser(skylines_file)
baskets = parser.load_baskets()

# We also get the nomenclature
# nomenclature_file = os.path.join(expe_params['file_directory'], "nomenclature.json")
# with open(nomenclature_file) as nomenclature_data:
#     nomenclature = json.load(nomenclature_data)

# We create the output file
with open(os.path.join(results_dir, "sky_signatures.txt"), "w") as res:
    res.write("")

count = 0
for cust_id, cust_baskets in baskets.items():
    if count < expe_params['nb_custs']:
        # We preprocess baskets to have them cleaned: empty baskets are removed and baskets with same timestamp are merged
        cust_baskets = [b for b in cust_baskets if b["products"]]
        i = 0
        while i < len(cust_baskets) - 1:
            if cust_baskets[i]["timestamp"] == cust_baskets[i + 1]["timestamp"]:
                cust_baskets[i]["products"] = cust_baskets[i]["products"] + cust_baskets[i + 1]["products"]
                del cust_baskets[i + 1]
            else:
                i += 1

        cust_baskets.sort(key=lambda x: x["timestamp"])
        complexities = []
        prod_counts = collections.Counter([p for b in cust_baskets for p in b["products"]])
        for k in range(1, max(list(prod_counts.values())) + 1):
            complexities.append(
                math.pow(2, len([p for p in prod_counts.values() if p >= k])) + k * len(cust_baskets) ** 2)
        optimal_index = complexities.index(min(complexities))+1
        skyDP = compute_sky_signature(cust_baskets, signature_jar_path, optimal_index,
                                      runner="fr.liglab.datalyse.retail.runners.RunSkySignatureDPComputation")
        skyPatterns = compute_sky_signature(cust_baskets, signature_jar_path, optimal_index,
                                            runner="fr.liglab.datalyse.retail.runners.RunSkySignatureComputation")
        skyDP.extend(skyPatterns)

        cust = {}
        cust["baskets"] = cust_baskets
        cust["signatures"] = skyDP
        cust["clientId"] = cust_id

        with open(os.path.join(results_dir, "sky_signatures.txt"), "a") as res:
            json.dump(cust, res)
            res.write("\n")

        count += 1
