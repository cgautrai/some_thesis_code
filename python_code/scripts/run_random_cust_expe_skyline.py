# This script will compute mdl best encodings using the greedy vs the skyline algorithm
import sys

sys.path.append("/udd/cgautrai/workspace/repo_these/")


from code_these_clement.utils.mdl_signature_mining import MDLSignatureSkyline
from code_these_clement.generators import signature_generators
import os

import json

import datetime
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--min_nb_segments', dest='min_nb_segments', type=int, help='Min number of segments for the generated customer')
parser.add_argument('--max_nb_segments', dest='max_nb_segments', type=int, help='Max number of segments for the generated customer')
parser.add_argument('--min_segment_lengths', dest='min_segment_lengths', type=int, help='Minimum size of a segment')
parser.add_argument('--max_segment_lengths', dest='max_segment_lengths', type=int, help='Maximum size of a segment')
parser.add_argument('--nb_products', dest='nb_products', type=int, help='Number of signature products')
parser.add_argument("--output_dir", dest="output_dir", type=str)
parser.add_argument('--noise_rate', dest='noise_rate', type=int, help='Noise rate')
parser.set_defaults(min_nb_segments=5, max_nb_segments=20, min_segment_lengths=2, max_segment_lengths=20, nb_products=30, noise_rate=0)
args = parser.parse_args()

# We set up the experiments in here and save it in the right place
now = datetime.datetime.now()
# expe_dir_name = os.path.join(args.output_dir, "experiments/" + "mcts_algorithm_compute_" + str(now.year) + "_" + str(now.month) + "_" + str(now.day) + "_" + str(now.hour) + "_" + str(now.minute))
expe_dir_name = args.output_dir
if not os.path.exists(expe_dir_name):
    os.makedirs(expe_dir_name)

results_dir = os.path.join(expe_dir_name, "results")
if not os.path.exists(results_dir):
    os.makedirs(results_dir)

overall_model_list = ["1_stream_uniform", "1_stream_prequential", "2_streams_uniform_uniform", "2_streams_uniform_prequential","2_streams_prequential_uniform","2_streams_prequential_prequential"]

expe_params = {}
expe_params["model_list"] = ["2_streams_uniform_uniform", "2_streams_prequential_prequential"]
# expe_params["model_list"] = ["2_streams_uniform_uniform"]
expe_params["min_nb_segments"] = args.min_nb_segments
expe_params["max_nb_segments"] = args.max_nb_segments
expe_params["min_segment_lengths"] = args.min_segment_lengths
expe_params["max_segment_lengths"] = args.max_segment_lengths
expe_params["nb_products"] = args.nb_products
expe_params["noise_rate"] = args.noise_rate/100.0
expe_params["signature_jar_path"] = "/udd/cgautrai/workspace/repo_these/code_these_clement/resources/signatures.jar"

expe_params["output_dir"] = args.output_dir

expe_params_file = os.path.join(expe_dir_name, "expe_params.txt")
with open(expe_params_file, "w") as f:
    json.dump(expe_params, f)

# for noise_rate in expe_params["noise_rates"]:
#     for add_rate in expe_params["signature_add_rates"]:
#         for items_ratio in expe_params["noise_items_ratio"]:
noise_rate = expe_params["noise_rate"]
items_ratios = [10, 20, 50]
add_rates = [0]

for items_ratio in items_ratios:
    for add_rate in add_rates:
        c, sign_prods, seg_length = signature_generators.generate_signature_add_rates([expe_params["min_nb_segments"], expe_params["max_nb_segments"]], [expe_params["min_segment_lengths"], expe_params["max_segment_lengths"]], expe_params["nb_products"], noise_rate, add_rate, items_ratio)
        seg_length = seg_length.tolist()
        for i,m in enumerate(overall_model_list):
            if m in expe_params["model_list"]:
                skyline_algo = MDLSignatureSkyline(c["baskets"], [m], i,
                                                   signature_jar_path=expe_params["signature_jar_path"],
                                                   optimal_dp=True)
                sign = skyline_algo.get_best_signature()
                best_cost = skyline_algo.get_best_cost()

                cust = {"signature":sign, "real_sign_prods":sign_prods, "real_sign_segments_length":seg_length, "add_rate":add_rate, "noise_rate":noise_rate,
                        "model": m, "items_ratio":items_ratio, "mdl_cost":best_cost}

                with open(os.path.join(results_dir, "expe_res_skyline.json"), "a") as f:
                    f.write(json.dumps(cust) + "\n")

